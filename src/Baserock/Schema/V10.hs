{-# LANGUAGE TemplateHaskell #-}
-----------------------------------------------------------------------------
-- |
-- Module     : Baserock.Schema.V10.Data
-- Copyright  : (c) Daniel Firth 2018
-- License    : BSD3
-- Maintainer : locallycompact@gmail.com
-- Stability  : experimental
--
-- This file defines the V10 Baserock Yaml Schema in Haskell
--
-----------------------------------------------------------------------------
module Baserock.Schema.V10
  ( module Data.Yaml.Pretty.Extras
  ,

  -- * Schema
    ChunkInstructions(..)
  , Chunk(..)
  , StratumInclude(..)
  , Stratum(..)
  , System(..)

  -- * Lenses
  , chunkInstructionsName
  , buildSystem
  , preConfigureCommands
  , configureCommands
  , postConfigureCommands
  , preBuildCommands
  , buildCommands
  , postBuildCommands
  , preInstallCommands
  , installCommands
  , postInstallCommands
  , chunkName
  , chunkMorph
  , repo
  , ref
  , sha
  , buildMode
  , chunkBuildSystem
  , stratumName
  , stratumDescription
  , stratumBDs
  , chunks
  , stratumIncludeName
  , stratumIncludeMorph
  , systemName
  , systemDescription
  , arch
  , strata
  , configurationExtensions
  )
where

import           Data.Yaml.Pretty.Extras
import           Lens.Micro.Platform     hiding ( (.=) )
import           RIO
import           RIO.List
import qualified RIO.Text                      as Text

-- Import Chunk and ChunkInstructions from V9
import           Baserock.Schema.V9             ( ChunkInstructions(..)
                                                , Chunk(..)
                                                , chunkInstructionsName
                                                , buildSystem
                                                , preConfigureCommands
                                                , configureCommands
                                                , postConfigureCommands
                                                , preBuildCommands
                                                , buildCommands
                                                , postBuildCommands
                                                , preInstallCommands
                                                , installCommands
                                                , postInstallCommands
                                                , chunkName
                                                , chunkMorph
                                                , repo
                                                , ref
                                                , sha
                                                , buildMode
                                                , chunkBuildSystem
                                                )



possibly f v = if v == mzero then mzero else [f .= v]

-- Stratum ("stratum.morph")

data Stratum = Stratum {
  _stratumName        :: Text,
  _stratumDescription :: Maybe Text,
  _stratumBDs         :: [Text],
  _chunks             :: [Chunk]
} deriving (Eq, Show)

$(makeLenses ''Stratum)

instance FromJSON Stratum where
  parseJSON (Object v) = Stratum
    <$> v .:  "name"
    <*> v .:? "description"
    <*> v .:? "build-depends" .!= []
    <*> v .:  "chunks"

instance ToJSON Stratum where
  toJSON x = object $ ["name" .= _stratumName x, "kind" .= ("stratum" :: Text), "chunks" .= _chunks x]
                   <> possibly "description" (_stratumDescription x)
                   <> possibly "build-depends" (_stratumBDs x)

instance ToPrettyYaml Stratum where
  fieldOrder = const ["name", "kind", "description", "morph", "repo", "ref", "sha", "build-mode", "build-system", "build-depends", "chunks"]

-- System ("system.morph")

data StratumInclude = StratumInclude {
  _stratumIncludeName  :: Text,
  _stratumIncludeMorph :: Text
} deriving (Eq, Show)

$(makeLenses ''StratumInclude)

instance FromJSON StratumInclude where
  parseJSON (Object v) = StratumInclude
    <$> v .: "name"
    <*> v .: "morph"

instance ToJSON StratumInclude where
  toJSON x = object ["name" .= _stratumIncludeName x, "morph" .= _stratumIncludeMorph x]

instance ToPrettyYaml StratumInclude where
  fieldOrder = const ["name", "morph"]

data System = System {
  _systemName              :: Text,
  _systemDescription       :: Maybe Text,
  _arch                    :: Text,
  _strata                  :: [StratumInclude],
  _configurationExtensions :: [Text]
} deriving (Eq, Show)

$(makeLenses ''System)

instance FromJSON System where
  parseJSON (Object v) = System
    <$> v .:  "name"
    <*> v .:? "description"
    <*> v .:  "arch"
    <*> v .:  "strata"
    <*> v .:  "configuration-extensions"

instance ToJSON System where
  toJSON x = object $ ["name" .= _systemName x, "kind" .= ("system" :: Text), "arch" .= _arch x, "strata" .= _strata x, "configuration-extensions" .= _configurationExtensions x]
                    <> possibly "description" (_systemDescription x)

instance ToPrettyYaml System where
  fieldOrder = const ["name", "morph", "kind", "description", "arch", "strata", "configuration-extensions"]
