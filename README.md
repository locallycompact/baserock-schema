# baserock-schema

Definitions schema for baserock system definitions. This repository provides a library
for interaction with definitions via lenses as well as several common tools for
assisting with annoying rebase work where your development branch and master have
diverged too much.

## Building

You will need [stack](https://docs.haskellstack.org/en/stable/README/)

You can build with

    stack build

## Testing

You can run the tests with

    stack test

## Installing

You can install the application by running

    stack install

which will install the baserock executable. You may then sit in a definitions repository and run

    baserock sanitize <path/to/morph>

## Usage

The main executable has several commands you can use to manipulate definitions in useful ways.
All commands which take a <morph> field can usually be given either a system or a stratum, and
in some but not all cases a chunk morph where it makes sense.

You can sanitize a system or stratum with

    baserock sanitize <path/to/morph>

You can set all the refs for every chunk in a given system or stratum by running

    baserock set-all-refs <ref> <path/to/morph>

You can use information from gitlab to modify your morphologies. To bump the sha of
every chunk to the HEAD of its currently listed ref, run

    baserock bump-shas <path/to/morph> -t GITLAB_API_TOKEN -u GITLAB_BASE_URL

You can also change the ref and track shas if and only if the supplied ref exists
on gitlab, by running

    baserock maybe-track-ref <ref> <path/to/morph> -t GITLAB_API_TOKEN -u GITLAB_BASE_URL

You can checkout the chunk instructions for every chunk in a system or stratum from a
different branch in the current definitoins repository, by running

    baserock steal-instructions <branch> <path/to/morph>
