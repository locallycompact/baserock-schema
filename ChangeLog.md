# Changelog for baserock-schema

## 0.0.3.5

Upgrade to yaml-0.8.31.1
Add maybe-track-ref for potentially tracking ref and updating sha
Add steal-instructions for pulling chunk instructions from a separate branch

## 0.0.3.4

Use hackage version of gitlab-api

## 0.0.3.3

Fix build-mode typo

## 0.0.3.2

Upgrade set-all-refs and bump-shas to work on system or stratum dynamically

## 0.0.3.1

Catch errors when sha is not found when updating.
Drop .git suffix when attempting to contact gitlab api

## 0.0.3.0

Switch to use Haskell-etc for cli options
Get basic lens functionality working
Drop MonadState database, it's not needed
Depend on yaml-pretty-extras or file lensing

## 0.0.2.0

Switch to use RIO for main loading systems
Add basic MonadState database for Definitions
Example sanitizer

## 0.0.1.5

* Fix some schema issues.
* Add lens interfaces.
* Add example application using gitlab-api

## 0.0.1.0

* Add Basic Chunk, Stratum and System parsing for baserock definitions V9
* Add "AST" decoders/encoders
* Add Stratum Graph representation
