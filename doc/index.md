---
title: Baserock Schema Utility Toolkit
---

# Baserock Schema Utility Toolkit

This package contains typesafe parsers, printers, AST codecs and graph traversals
for the Baserock Definitions format. It aims to simplify the problem of applying
modifications en mass to baserock systems and strata, to avoid problems that
commonly occur when trying to rebase large system changes.

[![baserock-schema on Hackage](https://img.shields.io/hackage/v/baserock-schema.svg?maxAge=86400)](https://hackage.haskell.org/package/baserock-schema)
